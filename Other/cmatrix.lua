--cmatrix made by JakobDev
--Licensed under BSD-2-Clause
local w,h = term.getSize()
local nSpeed = 0.01

local tChars = {"1","2","3","4","5","6","7","8","9","0","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","(",")","!","?","[","]","{","}","/","\\","#","=","<",">","*",",",".","@","%"}
local tLines = {}
for i=1,w do
    if math.random(1,2) == 1 then
        tLines[i] = {true,math.random(1,10)}
    else
        tLines[i] = {false,math.random(1,10)}
    end
end

local tText = {}
for i=0,h do
    tText[i] = ""
end

local function scrollDown()
    term.scroll(-1)
    for i=h-1,1,-1 do
        tText[i] = tText[i-1]
    end
    tText[h] = tText[h-1]
    tText[h+1] = nil
end

local function generateText()
    local sText = ""
    for i=1,w do
        if  tLines[i][1] == true then
            sText = sText..tChars[math.random(1,#tChars)]
        else
            sText = sText.." "
        end
        tLines[i][2] = tLines[i][2] - 1
        if tLines[i][2] == 0 then
            if math.random(1,2) == 1 then
                tLines[i] = {true,math.random(1,10)}
            else
                tLines[i] = {false,math.random(1,10)}
            end
        end
    end
    term.setCursorPos(1,1)
    term.write(sText)
    tText[1] = sText
end

local function redrawLines()
    --For cahnging Colours
    for i=1,h do
        term.setCursorPos(1,i)
        term.write(tText[i])
    end
end

local tPalette = {}
if term.getPaletteColour then
    for k,v in pairs(colors) do
        if type(v) == "number" then
            tPalette[k] = table.pack(term.getPaletteColour(v))
        end
    end
    term.setPaletteColor(colors.green,30/255,197/255,3/255)
    term.setPaletteColor(colors.black,0,0,0)
end

term.setBackgroundColor(colors.black)
term.setTextColor(colors.green)
term.clear()
generateText()

local nID = os.startTimer(nSpeed)

while true do
    local tEvent = table.pack(os.pullEvent())
    if tEvent[1] == "timer" and nID == tEvent[2] then
        scrollDown()
        generateText()
        nID = os.startTimer(nSpeed)
    elseif tEvent[1] == "char" then
        local sKey = string.upper(tEvent[2])
        if sKey == "Q" then
            bBreak = true
            break
        elseif sKey == "0" then
            nSpeed = 0.01
        elseif sKey == "1" then
            nSpeed = 0.02
        elseif sKey == "2" then
            nSpeed = 0.03
        elseif sKey == "3" then
            nSpeed = 0.04
        elseif sKey == "4" then
            nSpeed = 0.05
        elseif sKey == "5" then
            nSpeed = 0.06
        elseif sKey == "6" then
            nSpeed = 0.07
        elseif sKey == "7" then
            nSpeed = 0.08
        elseif sKey == "8" then
            nSpeed = 0.09
        elseif sKey == "9" then
            nSpeed = 0.1
        elseif sKey == "@" then
            term.setTextColor(colors.green)
            redrawLines()
        elseif sKey == "!" then
            if term.setPaletteColor then
                term.setPaletteColor(colors.red,1,0,0)
            end
            term.setTextColor(colors.red)
            redrawLines()
        elseif sKey == "%" then
            if term.setPaletteColor then
                term.setPaletteColor(colors.magenta,1,0,1)
            end
            term.setTextColor(colors.magenta)
            redrawLines()
        elseif sKey == "&" then
            if term.setPaletteColor then
                term.setPaletteColor(colors.white,1,1,1)
            end
            term.setTextColor(colors.white)
            redrawLines()
        elseif sKey == "$" then
            if term.setPaletteColor then
                term.setPaletteColor(colors.blue,0,0,1)
            end
            term.setTextColor(colors.blue)
            redrawLines()
        elseif sKey == "#" then
            if term.setPaletteColor then
                term.setPaletteColor(colors.yellow,1,1,0)
            end
            term.setTextColor(colors.yellow)
            redrawLines()
        elseif sKey == "^" then
            if term.setPaletteColor then
                term.setPaletteColor(colors.cyan,0,1,1)
            end
            term.setTextColor(colors.cyan)
            redrawLines()
        end
    end
end

if term.setPaletteColor then
    for k,v in pairs(tPalette) do
        term.setPaletteColor(colors[k],table.unpack(v))
    end
end

term.clear()
term.setCursorPos(1,1)
