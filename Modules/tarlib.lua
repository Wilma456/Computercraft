local expect = require("cc.expect").expect

local function generateZero(num)
    local s = ""
    for i=1,num do
        s = s .. "\000"
    end
    return s
end

--Source of to_decimal https://exercism.io/tracks/lua/exercises/octal/solutions/095cbe5e82ff4f888bf2d18d372e1781
local function to_decimal(s)
  local decimal = 0
  for i = 1, #s do
    local digit = tonumber(s:sub(i, i))
    if not digit or digit >= 8 then return 0 end
    decimal = decimal + digit * 8 ^ (#s - i)
  end
  return decimal
end

--Source of checksum_header https://github.com/mimetic/tar.lua/blob/master/tar.lua
local function checksum_header(block)
    local sum = 256
    for i = 1,148 do
        sum = sum + block:byte(i)
    end
    for i = 157,500 do
        sum = sum + block:byte(i)
    end
    return sum
end

local function replace_char(pos, str, r)
    return str:sub(1, pos-1) .. r .. str:sub(pos+1)
end

local function removeZero(sField)
    return sField:match("^[^%z]*")
end

local function open_read(sPath)
    local tHandle = setmetatable({},{})
    local tBlocks = {}
    local tFiles = {}
    local f = fs.open(sPath,"rb")
    local sLongname = ""
    local nCount = 1
    while true do
        local block = f.read(512)
        if block == nil then
            f.close()
            break
        end
        table.insert(tBlocks,block)
        --Check if the block just contains zero
        if checksum_header(block) ~= 256 then
            local sFilename
            if sLongname == "" then
                sFilename = removeZero(block:sub(1,100))
            else
                sFilename = sLongname
                sLongname = ""
            end
            local sTypebyte = block:sub(157,157)
            local sPrefix = removeZero(block:sub(346,500))
            if sPrefix ~= "" then
                sFilename = sPrefix .. "/" .. sFilename
            end
            if sTypebyte == "0" then
                local nStartBlock = nCount
                local sSizeBytes = block:sub(125,135)
                local nFileSize = to_decimal(sSizeBytes)
                local nRestSize = nFileSize
                while nRestSize > 512 do
                    table.insert(tBlocks,f.read(512))
                    nRestSize = nRestSize -512
                    nCount = nCount + 1
                end
                table.insert(tBlocks,f.read(512))
                nCount = nCount + 1
                table.insert(tFiles,{name=sFilename,startblock=nStartBlock,endblock=nCount,type="file",size=nFileSize,restsize=nRestSize})
                nCount = nCount + 1
            elseif sTypebyte == "5" then
                table.insert(tFiles,{name=sFilename,startblock=nCount,endblock=nCount,type="directory",size=0,restsize=0})
                nCount = nCount + 1
            elseif sTypebyte == "L" then
                sLongname = removeZero(f.read(512))
                nCount = nCount + 1
            else
                table.insert(tFiles,{name=sFilename,startblock=nCount,endblock=nCount,type="unknown",size=0,restsize=0})
                nCount = nCount + 1
            end
        end
    end
    function tHandle.list()
        local tList = {}
        for _,i in ipairs(tFiles) do
            table.insert(tList,i["name"])
        end
        return tList
    end
    function tHandle.type(sName)
        expect(1,sName,"string")
        for _,i in ipairs(tFiles) do
            if i["name"] == sName then
                return i["type"]
            end
        end
    end
    function tHandle.size(sName)
        expect(1,sName,"string")
        for _,i in ipairs(tFiles) do
            if i["name"] == sName then
                return i["size"]
            end
        end
    end
    function tHandle.read(sFile)
        expect(1,sFile,"string")
        for _,i in ipairs(tFiles) do
            if i["name"] == sFile then
                local sFilecontent = ""
                for n=i["startblock"]+1,i["endblock"]-1 do
                    sFilecontent = sFilecontent .. tBlocks[n]
                    --To avoid to long without yielding error it needed to sleep every 1000 turns
                    if n/1000 == math.floor(n/1000) then
                        sleep(0.1)
                    end
                end
                sFilecontent = sFilecontent .. tBlocks[i["endblock"]]:sub(1,i["restsize"])
                return sFilecontent
            end
        end
    end
    function tHandle.extract(sFile,sPath)
        expect(1,sFile,"string")
        expect(2,sPath,"string")
        sData = tHandle.read(sFile)
        f = fs.open(sPath,"wb")
        f.write(sData)
        f.close()
    end
    function tHandle.extractAll(sPath)
        expect(1,sPath,"string")
        for _,i in ipairs(tHandle.list()) do
            local sExtractPath = fs.combine(sPath,i)
            if tHandle.type(i) == "file" then
                tHandle.extract(i,sExtractPath)
            elseif tHandle.type(i) == "directory" then
                fs.makeDir(sExtractPath)
            end
        end
    end
    return tHandle
end

local function open_write(sPath,bAppend)
    local tHandle = setmetatable({},{})
    tBlocks = {}
    if bAppend then
        local f = fs.open(sPath,"rb")
        local bOldZero = false
        while true do
            sBlock = f.read(512)
            if sBlock == nil then
                break
            else
                --Check if a block is just zero
                if checksum_header(sBlock) == 256 then
                    if bOldZero == true then
                        --We don't need many zero blocks at the end
                        break
                    else
                        bOldZero = true
                    end
                else
                    bOldZero = false
                    table.insert(tBlocks,sBlock)
                end
            end
        end
        f.close()
    end
    local function createHeader(sName,nSize,sType,tOptions)
        if not tOptions then
            tOptions = {}
        end
        if #sName > 100 then
            createHeader("\000/\000/@LongLink",#sName,"L",tOptions)
            table.insert(tBlocks,sName .. generateZero(512-#sName))
        end
        sBlock = sName:sub(1,100)
        sBlock = sBlock .. generateZero(100-#sBlock)
        --Stuff like file mode or ids that we dont't need
        sBlock = sBlock .. "0000664\0000001750\0000001750\000"
        --Add filesize in octal
        sBlock = sBlock .. string.format("%11o",nSize):gsub(" ","0") .. "\000"
        --Add modification date in octal
         sBlock = sBlock .. string.format("%11o",(tOptions["modification"] or 0) / 1000):gsub(" ","0") .. "\000"
        --Checksum must be spaces. Replaced later.
        sBlock = sBlock .. "      \000"
        --sBlock = sBlock .. generateZero(1)
        --Typeflag
        sBlock = sBlock .. " " .. sType
        --Fill up
        sBlock = sBlock .. generateZero(100)
        --tar has the ustar type
        sBlock = sBlock .. "ustar  " .. "\000"
        --Username
        local sName = tOptions.username or "root"
        sBlock = sBlock .. sName
        sBlock = sBlock .. generateZero(32- #sName)
        --Groupname
        local sGroupname = tOptions.groupname or "root"
        sBlock = sBlock .. sGroupname
        sBlock = sBlock .. generateZero(32- #sGroupname)
        --sBlock = sBlock .. generateZero(16)
        --sBlock = sBlock .. "test"
        --Fill the rest
        sBlock = sBlock .. generateZero(512-#sBlock)
        local sChecksum = string.format("%6o",checksum_header(sBlock)):gsub(" ","0")
        local nCount = 1
        for i = 149,154 do
            sBlock = replace_char(i,sBlock,sChecksum:sub(nCount,nCount))
            nCount = nCount + 1
        end
        table.insert(tBlocks,sBlock)
    end
    function tHandle.write(sName,sContent,tOptions)
        expect(1,sName,"string")
        expect(2,sContent,"string")
        expect(3,tOptions,"table","nil")
        if not tOptions then
            tOptions = {}
        end
        createHeader(sName,#sContent,"0",tOptions)
        if #sContent == 0 then
            table.insert(tBlocks,sContent .. generateZero(512))
        end
        while #sContent > 512 do
            table.insert(tBlocks,sContent:sub(1,512))
            sContent = sContent:sub(513)
        end
        if #sContent > 0 then
            table.insert(tBlocks,sContent .. generateZero(512- #sContent))
        end
    end
    function tHandle.makeDirectory(sName,tOptions)
        expect(1,sName,"string")
        expect(2,tOptions,"table","nil")
        if not tOptions then
            tOptions = {}
        end
       createHeader(sName,0,"5",tOptions)
    end
    function tHandle.addFile(sPath)
        expect(1,sPath,"string")
        local attr = fs.attributes(sPath)
        local f = fs.open(sPath,"rb")
        tHandle.write(sPath,f.readAll() or "",attr)
        f.close()
    end
    function tHandle.addDirectory(sPath)
        expect(1,sPath,"string")
        local attr = fs.attributes(sPath)
        tHandle.makeDirectory(sPath,attr)
        for _,i in ipairs(fs.list(sPath)) do
            sFullPath = fs.combine(sPath,i)
            if fs.isDir(sFullPath) then
                tHandle.addDirectory(sFullPath)
            else
                tHandle.addFile(sFullPath)
            end
        end
    end
    function tHandle.close()
        for i=#tBlocks,19 do
            table.insert(tBlocks,generateZero(512))
        end
        f = fs.open(sPath,"wb")
        for _,i in ipairs(tBlocks) do
            f.write(i)
        end
        f.close()
    end
    return tHandle
end

local function open(sPath,sMode)
    if sMode == "r" then
        if not fs.exists(sPath) or fs.isDir(sPath) then
            error(sPath .. ": No such file",2)
        end
        return open_read(sPath)
    elseif sMode == "w" then
        return open_write(sPath,false)
    elseif sMode == "a" then
        if not fs.exists(sPath) or fs.isDir(sPath) then
            error(sPath .. ": No such file",2)
        end
        return open_write(sPath,true)
    else
        error("Unsupported mode",2)
    end
end

local function version()
    return "2.0"
end

return {
    open=open,
    version=version
}
