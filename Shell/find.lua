local Args = ...

os.loadAPI("/usr/apis/jakobapi")

local fileta
if Args == nil then
  fileta = jakobapi.listAllFiles()
else
  fileta = jakobapi.listAllFiles(Args)
end

for _,filename in ipairs(fileta) do
print(filename)
end
